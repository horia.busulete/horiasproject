#include  "timer.h"
#include <stdio.h>
#include <stdlib.h>


void functie_test()
{
int i,j,k; 
for(i=1;i<=200000;i++) 
	for(j=1;j<=20000;j++) 
		if(i%2)k=i; 
}
int fib1(int n)
{
	if (n == 0)
		return 1;
	if (n == 1)
		return 1;
	return fib1(n - 1) + fib1(n - 2);
}

int fib2(int n)
{
	int F[1000];
	if (n == 0)
		F[n] = 1;
	if (n == 1)
		F[n] = 1;
	if (F[n] == 0)
		F[n] = fib2(n - 1) + fib2(n - 2);
	return F[n];
}

int fib3(int n)
{

	int F[1000] = { 0 };
	F[0] = 1;
	F[1] = 1;
	for (int i = 2; i <= n; i++)
		F[i] = F[i - 1] + F[i - 2];

	return F[n];
}

int factorial(int n)

{
	if (n == 0)
		return 1;
	return  n * factorial(n - 1);
}

int factorial1(int n) {
	int f = 1;
	for (int i = 1; i <= n; i++) {
		f = f*i;
	}
	return f;
}

void main(void)
{ float timp;
int n;
scanf("%d", &n);


/*starton();
fib1(n);
timp = startoff();
printf("%f", timp);


starton();
fib2(n);
timp = startoff();
printf("%f", timp);

starton();
fib3(n);
timp = startoff();
printf("%f", timp);

*/

starton();
factorial(n);
timp = startoff();
printf("%f", timp);
printf("%d/n");
starton();
factorial1(n);
printf("%f", timp);
} 

