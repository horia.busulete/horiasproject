#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>

#define MAXCHAR 1000
struct Linie
{
    char cuvant[MAXCHAR];
    int numar_aparitii;
    float frecventa_aparitii;
    float cuvinte_totale;
};

static int
cmpstringpalf(const void *p1, const void *p2)
{
    struct Linie *p01 = (struct Linie *)p1;
    struct Linie *p02 = (struct Linie *)p2;
    return strcmp(p01->cuvant, p02->cuvant);
}

static int
cmpstringpinvalf(const void *p1, const void *p2)
{
    struct Linie *p01 = (struct Linie *)p1;
    struct Linie *p02 = (struct Linie *)p2;
    return strcmp(p02->cuvant, p01->cuvant);
}

static int
cmpstringpnrtotalcuvinte(const void *p1, const void *p2)
{
    struct Linie *p01 = (struct Linie *)p1;
    struct Linie *p02 = (struct Linie *)p2;
    return p02->cuvinte_totale - p01->cuvinte_totale;
}

int main()
{
    struct Linie Tablou[1000];
    int dimensiune_Tablou = 0;
    int contor;
    FILE *myFile = fopen("random_ex1.csv", "r");
    char buffer[1000];
    int contor_linie = 0;
    while (fgets(buffer, 1000, myFile))
    {

        contor_linie++;
        if (contor_linie == 1)
            continue;
        char *token = strtok(buffer, ",");
        contor = 0;
        while (token)
        {
            contor++;

            if (contor == 1)
                strcpy(Tablou[dimensiune_Tablou].cuvant, token);
            if (contor == 2)
                Tablou[dimensiune_Tablou].numar_aparitii = atoi(token);
            if (contor == 3)
                Tablou[dimensiune_Tablou].frecventa_aparitii = atof(token);

            token = strtok(NULL, ",");
        }
        dimensiune_Tablou++;
    }
    double frecventa_minima = 1;
    for (int i = 0; i < dimensiune_Tablou; i++)
        if (Tablou[i].frecventa_aparitii < frecventa_minima)
            frecventa_minima = Tablou[i].frecventa_aparitii;

    printf("%.2f:", frecventa_minima);

    qsort(Tablou, 1000, sizeof(struct Linie), cmpstringpalf);

    for (int i = 0; i < dimensiune_Tablou; i++)
        if (Tablou[i].frecventa_aparitii == frecventa_minima)
            printf("%s ", Tablou[i].cuvant);

    printf("\n");

    qsort(Tablou, 1000, sizeof(struct Linie), cmpstringpinvalf);

    int numar_maxim_aparitii = -1;
    for (int i = 0; i < dimensiune_Tablou; i++)
        if (Tablou[i].numar_aparitii > numar_maxim_aparitii)
            numar_maxim_aparitii = Tablou[i].numar_aparitii;

    printf("%d:", numar_maxim_aparitii);

    for (int i = 0; i < dimensiune_Tablou; i++)
        if (Tablou[i].numar_aparitii == numar_maxim_aparitii)
            printf("%s ", Tablou[i].cuvant);

    printf("\n");

    for (int i = 0; i < dimensiune_Tablou; i++)
        if (Tablou[i].frecventa_aparitii != 0)
            Tablou[i].cuvinte_totale = Tablou[i].numar_aparitii / Tablou[i].frecventa_aparitii;

    clock_t start = clock();

    qsort(Tablou, 1000, sizeof(struct Linie), cmpstringpnrtotalcuvinte);

    clock_t stop = clock();

    double timp_trecut_qsort = (double)(stop - start) * 1000.0 / CLOCKS_PER_SEC;

    qsort(Tablou, 1000, sizeof(struct Linie), cmpstringpinvalf);

    start = clock();

    for (int i = 0; i < dimensiune_Tablou - 1; i++)
        for (int j = i + 1; j < dimensiune_Tablou; j++)
            if (Tablou[i].cuvinte_totale < Tablou[j].cuvinte_totale)
            {
                float auxiliar;
                auxiliar = Tablou[j].cuvinte_totale;
                Tablou[j].cuvinte_totale = Tablou[i].cuvinte_totale;
                Tablou[i].cuvinte_totale = auxiliar;
            }
    stop = clock();

    double timp_trecut_bubblesort = (double)(stop - start) * 1000.0 / CLOCKS_PER_SEC;

    for (int i = 0; i < dimensiune_Tablou; i++)
        printf("%s  %.2f\n", Tablou[i].cuvant, Tablou[i].cuvinte_totale);

    printf("%.7f  %.7f", timp_trecut_qsort, timp_trecut_bubblesort);
}
